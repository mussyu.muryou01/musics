import argparse
import json
import os
import traceback
import requests

import tqdm
import threading
import thread_manager

import chart

try:
    import api
except ImportError:
    api = None

note_sizes = {
    'easy': 2.0,
    'normal': 1.5,
    'hard': 1.25,
    'expert': 1.0,
    'master': 0.875,
}


score_host = 'https://asset3.pjsekai.moe'
score_file_name = 'public/%04d_01/%s'


def parse(music_id, difficulty):
    file_name = score_file_name % (music_id, difficulty)
    try:
        with open(file_name) as f:
            lines = f.readlines()
    except:
        url = '%s/music/music_score/%04d_01/%s' % (score_host, music_id, difficulty)
        print('fetching score from %s' % url)
        r = requests.get(url)
        if r.status_code != 200:
            return
        lines = r.text.splitlines()

    if api:
        music = api.Sekai.get_database('musics', key='id')[music_id]
        for music_difficulty in api.Sekai.get_database('musicDifficulties'):
            if music_difficulty['musicId'] == music_id and music_difficulty['musicDifficulty'] == difficulty:
                break

        if music['composer'] == music['arranger']:
            artist = music['composer']
        elif music['composer'] in music['arranger']:
            artist = music['arranger']
        elif music['arranger'] in music['composer']:
            artist = music['composer']
        else:
            artist = '%s / %s' % (music['composer'], music['arranger'])

    sus = chart.SUS(
        lines,
        note_size=note_sizes[difficulty],
        note_host=args.note_host,
        **({
            'title': music['title'],
            'artist': artist,
            'difficulty': difficulty,
            'playlevel': music_difficulty['playLevel'],
            'jacket': 'https://asset3.pjsekai.moe/music/jacket/%s/%s.webp' % (music['assetbundleName'], music['assetbundleName'])
        } if api else {}),
    )

    try:
        with open('rebases.json') as f:
            rebases: list = json.load(f)
        for rebase in rebases:
            if rebase['musicId'] == music_id:
                break
        else:
            raise NotImplementedError
    except:
        rebase = None
        # utils.get_database('pjsekai_musics')['musicRebases'].find_one({
        #     'musicId': music_id,
        # })

    if rebase:
        sus.score = sus.score.rebase([
            chart.Event(
                bar=event.get('bar'),
                bpm=event.get('bpm'),
                bar_length=event.get('barLength'),
                sentence_length=event.get('sentenceLength'),
                section=event.get('section'),
            )
            for event in rebase.get('events', [])
        ], offset=rebase.get('offset', 0))

    file_name = score_file_name % (music_id, difficulty)
    os.makedirs(os.path.dirname(file_name), exist_ok=True)

    with open('chart/css/sus.css') as f:
        style_sheet = f.read()

    with open('chart/css/%s.css' % difficulty) as f:
        style_sheet += '\n' + f.read()

    sus.export(file_name + '.svg', style_sheet=style_sheet)

    try:
        import cairosvg
    except (ImportError, OSError):
        cairosvg = None

    if cairosvg and hasattr(cairosvg, 'svg2png'):
        cairosvg.svg2png(url=file_name + '.svg', write_to=file_name + '.png')


def handle(music_ids=None):
    threading.Thread(target=thread_manager.start_thread, args=(thread_manager.thread, parse, 16)).start()

    if music_ids is None:
        musics = api.Sekai.get_database('musics', key='id').values()
        thread_manager.progress.total = len(musics) * 5

        for music in api.Sekai.get_database('musics', key='id').values():
            for difficulty in [
                'easy',
                'normal',
                'hard',
                'expert',
                'master',
            ]:
                thread_manager.pool.put((music['id'], difficulty))

    else:
        thread_manager.progress.total = len(music_ids) * 5
        for music_id in music_ids:
            for difficulty in [
                'easy',
                'normal',
                'hard',
                'expert',
                'master',
            ]:
                thread_manager.pool.put((music_id, difficulty))

    thread_manager.pool.join()


if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument('--note_host', default='https://asset3.pjsekai.moe/notes')
    args = parser.parse_args()

    handle()
