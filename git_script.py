import io
import os
import git
import json
import datetime
import requests_oauthlib

import api


def push():
    repo = git.Repo('public')

    try:
        repo.git.add(all=True)
        repo.git.commit('-m', 'auto update db %s' % datetime.datetime.now().strftime("%Y-%m-%d"))
        repo.git.push()
        return True
    except:
        return False


def get_blob_json(repo: git.Repo, commit, file):
    if commit:
        blob = repo.commit(commit).tree / file
        with io.BytesIO(blob.data_stream.read()) as f:
            a = json.load(f)
    else:
        file = repo._working_tree_dir + '/' + file
        with open(file) as f:
            a = json.load(f)

    return a


def diff(new_commit='HEAD', old_commit='HEAD~1'):
    repo = git.Repo('public')
    new_music_difficulties = get_blob_json(repo, new_commit, 'musicDifficulties.json')
    old_music_difficulties = get_blob_json(repo, old_commit, 'musicDifficulties.json')

    new_music_difficulties = {
        (music_difficulty['musicId'], music_difficulty['musicDifficulty']): music_difficulty
        for music_difficulty in new_music_difficulties
    }

    old_music_difficulties = {
        (music_difficulty['musicId'], music_difficulty['musicDifficulty']): music_difficulty
        for music_difficulty in old_music_difficulties
    }

    changes = []

    for music_difficulty in new_music_difficulties.values():
        # if music_difficulty['musicDifficulty'] in ['easy', 'normal']:
        #     continue

        if 'playLevelAdjust' not in music_difficulty:
            continue

        old_music_difficulty = old_music_difficulties.get(
            (music_difficulty['musicId'], music_difficulty['musicDifficulty']), {})
        level_adjust_difference = (
            round(music_difficulty['playLevelAdjust'], 1) -
            round(old_music_difficulty.get('playLevelAdjust', 0), 1)
        )

        if 'playLevelAdjust' not in old_music_difficulty or abs(level_adjust_difference) >= {
            'easy': 0.5,
            'normal': 0.4,
            'hard': 0.3,
            'expert': 0.2,
            'master': 0.1,
        }.get(music_difficulty['musicDifficulty'], 0.0):
            changes.append({
                'musicId': music_difficulty['musicId'],
                'musicDifficulty': music_difficulty['musicDifficulty'],
                'playLevelAdjustFrom': old_music_difficulty.get('playLevelAdjust', None),
                'playLevelAdjustTo': music_difficulty['playLevelAdjust']
            })

    if not changes:
        return [], []

    old_changes_groups = {}
    new_changes_groups = {}

    for change in changes:
        changes_groups = old_changes_groups if change['playLevelAdjustFrom'] is not None else new_changes_groups
        if change['musicId'] not in changes_groups:
            changes_groups[change['musicId']] = {
                'musicId': change['musicId'],
                'changes': [],
            }
        changes_groups[change['musicId']]['changes'].append(change)

    return old_changes_groups, new_changes_groups


def tweet(changes_groups, title='Level adjust automatic estimation of songs in #ProjectSekaiProfile\n', test=False):
    if not changes_groups:
        return

    musics = api.Sekai.get_database('musics', key='id')
    music_difficulties = {
        (music_difficulty['musicId'], music_difficulty['musicDifficulty']): music_difficulty
        for music_difficulty in api.Sekai.get_database('musicDifficulties')
    }

    outputs = []
    outputs.append(title)

    for changes_group in changes_groups.values():
        output = ''
        output += '🎶 %s\n' % (musics[changes_group['musicId']]['title'])
        for change in changes_group['changes']:
            level = music_difficulties[(change['musicId'], change['musicDifficulty'])]['playLevel']
            output += '[%s%s %s] %s → %s %s\n' % (
                {
                    'easy': '🟢',
                    'normal': '🔵',
                    'hard': '🟡',
                    'expert': '🔴',
                    'master': '🟣',
                }.get(change['musicDifficulty'], ''),
                change['musicDifficulty'],
                level,
                '%.1f' % (level + change['playLevelAdjustFrom'])
                if change['playLevelAdjustFrom'] is not None else '?',
                '%.1f' % (level + change['playLevelAdjustTo']),
                '↑' if change['playLevelAdjustTo'] > (change['playLevelAdjustFrom'] or 0) else '↓',
            )
        outputs.append(output)

    tweets = []
    tweet = ''

    for output in outputs:
        if len(tweet + output + '\n') > 200:
            tweets.append(tweet)
            tweet = ''

        tweet += output + '\n'
    else:
        tweets.append(tweet)

    for tweet in tweets:
        print(tweet)
        print('--------------------')

    if test:
        return

    oauth = requests_oauthlib.OAuth1Session(
        client_key=os.environ.get('TWITTER_API_KEY'),
        client_secret=os.environ.get('TWITTER_API_KEY_SECRET'),
        resource_owner_key=os.environ.get('TWITTER_ACCESS_TOKEN'),
        resource_owner_secret=os.environ.get('TWITTER_ACCESS_TOKEN_SECRET'),
    )

    last_tweet_id = None
    for tweet in tweets:
        response = oauth.post('https://api.twitter.com/2/tweets', json={
            'text': tweet,
            **({'reply': {
                'in_reply_to_tweet_id': last_tweet_id,
            }} if last_tweet_id else {})
        })
        print(response.json())
        last_tweet_id = response.json()['data']['id']


if __name__ == '__main__':
    test = False
    if push():
        old_changes_groups, new_changes_groups = diff('HEAD', 'HEAD~1')
        tweet(old_changes_groups, title='Level adjust automatic estimation of existing songs in #ProjectSekaiProfile\n', test=test)
        tweet(new_changes_groups, title='Level adjust automatic estimation of NEW songs in #ProjectSekaiProfile\n', test=test)
