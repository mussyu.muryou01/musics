# Project Sekai Musics

This repo contains scripts to process infomation of musics in Project Sekai, containing:

* Popularity

* Relative difficulty

* BPM

The results are pushed to [this repo](https://gitlab.com/pjsekai/database/musics) automatically at the end of each event. The static distribution version can be get under musics.pjsekai.moe. 
